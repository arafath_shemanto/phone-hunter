// loadPhones
const loadPhones = () => {
  let inputText = document.getElementById("input_field").value;
  //   url using search input
  if (inputText.length === 0) {
    console.log("type something");
    document.getElementById("error_text").innerText = "Search Something";
    errorShow("block");
  } else {
    let url = `https://openapi.programming-hero.com/api/phones?search=${inputText}`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => showPhones(data));
  }
  // clear
  document.getElementById("input_field").value = "";
};

// error showing function
const errorShow = (displayType) => {
  document.getElementById("error_text").style.display = displayType;
};
// showPhones
const showPhones = (phones) => {
  if (phones.data.length === 0) {
    errorShow("block");
    document.getElementById("error_text").innerText = "No Data Found";
  }
  let display_box = document.getElementById("display_wrapper");
  display_box.textContent = "";
  for (let phone of phones.data.slice(0, 20)) {
    let div = document.createElement("div");
    div.classList.add("col-xl-4", "col-md-6");
    div.innerHTML = `
        <div class="card mb-4">
            <div class="thumb">
                <img src="${phone.image}" class="img-fluid" alt="...">
            </div>
            <div class="card-body">
                <h5 class="card-title">Phone Name: ${phone.phone_name}</h5>
                <p class="card-text">Phone Brand: ${phone.brand}</p>
                <button class="primray_btn" onclick="phoneDetails('${phone.slug}')">More Details</button>
            </div>
        </div>
    `;
    display_box.appendChild(div);
    errorShow("none");
  }
};

// phoneDetails using id
let phoneDetails = (phoneId) => {
  let url = `https://openapi.programming-hero.com/api/phone/${phoneId}`;
  fetch(url)
    .then((res) => res.json())
    .then((data) => phoneDetailsShow(data));
};

// phoneDetailsShow function
const phoneDetailsShow = (phone) => {
  console.log(phone.data);
  console.log(phone.data.image);
  //   othersArray
  let preview_box = document.getElementById("phone_details_preview");

  //   clear prev details
  preview_box.textContent = "";
  let div = document.createElement("div");
  div.classList.add("card", "mb-4", "details_card");
  div.innerHTML = `
  <div class="thumb">
    <img src="${phone.data.image}" class="img-fluid" alt="...">
  </div>
  <div class="card-body">
      <h5 class="card-title"> <span class="fw-bold">Phone Name:</span> ${
        phone.data.name
      }</h5>
      <p class="card-text"><span class="fw-bold">Brand Name:</span> ${
        phone.data.brand
      }</p>
      <p class="card-text"> <span class="fw-bold">ReleaseDate:</span>  
      ${
        phone.data.releaseDate
          ? phone.data.releaseDate
          : "Have't any release date"
      }</p>
      <span class="fw-bold d-block ">MainFeatures</span>

      <p class="m-0">
       <span class="font_medium">Storage:</span>
      ${
        phone.data.mainFeatures?.storage
          ? phone.data.mainFeatures.storage
          : "storage not support"
      }</p>

      <p class="m-0">
       <span class="font_medium">displaySize:</span>
      ${
        phone.data.mainFeatures?.displaySize
          ? phone.data.mainFeatures.displaySize
          : "displaySize not support"
      }</p>

      <p class="m-0">
      <span class="font_medium">ChipSet:</span>
      ${
        phone.data.mainFeatures?.chipSet
          ? phone.data.mainFeatures.chipSet
          : "chipSet not support"
      }</p>

      <p class="m-0">
      <span class="font_medium">Memory:</span>
      ${
        phone.data.mainFeatures?.memory
          ? phone.data.mainFeatures.memory
          : "memory not support"
      }</p>

      <p class="">
      <span class="font_medium">sensors:</span>
      ${
        phone.data.mainFeatures?.sensors
          ? phone.data.mainFeatures.sensors
          : "sensors not support"
      }</p>


      <span class="fw-bold d-block ">Others:</span> 
      <p class="m-0">
        <span class="font_medium"> WLAN :</span>
        ${phone.data.others?.WLAN ? phone.data.others.WLAN : "WLAN not support"}
      </p>
      <p class="m-0"> <span class="font_medium">Bluetooth :</span> ${
        phone.data.others?.Bluetooth
          ? phone.data.others.Bluetooth
          : "Bluetooth not support"
      }</p>
      <p class="m-0"> <span class="font_medium">GPS :</span> ${
        phone.data.others?.GPS ? phone.data.others.GPS : "GPS not support"
      }</p>
      <p class="m-0"> <span class="font_medium">NFC :</span> ${
        phone.data.others?.NFC ? phone.data.others.NFC : "NFC not support"
      }</p>
      <p class="m-0"> <span class="font_medium">Radio :</span> ${
        phone.data.others?.Radio ? phone.data.others.Radio : "Radio not support"
      }</p>
  </div>
  `;
  preview_box.appendChild(div);
};
